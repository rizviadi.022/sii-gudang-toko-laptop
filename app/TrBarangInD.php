<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrBarangInD extends Model
{
    protected $table='tr_barang_in_d';
    protected $fillable=[
        'notrans_barang_in',
        'kode_barang',
        'nama_barang',
        'jumlah',
        'kode_gudang'
    ];
}
