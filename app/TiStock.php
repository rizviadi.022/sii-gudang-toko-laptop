<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TiStock extends Model
{
    protected $table='ti_stock';
    protected $fillable=[
        'kode_barang',
        'nama_barang',
        'stock_akhir',
        'kode_gudang'
    ];
}
