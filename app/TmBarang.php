<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TmBarang extends Model
{
  protected $table='tm_barang';
  protected $fillable=[
      'kode_barang',
      'nama_barang',
      'kode_grup',
      'keterangan',
      'default_gd',
      'aktif'
  ];
}
