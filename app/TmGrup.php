<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TmGrup extends Model
{
    protected $table='tm_grup';
    protected $fillable=[
        'kode_grup',
        'nama_grup',
        'keterangan',
        'aktif'
    ];
}
