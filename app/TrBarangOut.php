<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrBarangOut extends Model
{
   protected $table='tr_barang_out';
   protected $fillable=[
     'notrans_barang_out',
     'tanggal',
     'kode_toko'
   ];
}
