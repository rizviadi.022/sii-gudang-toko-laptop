<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TempTransOut extends Model
{
    protected $table='temp_transout';
    protected $fillable=[
        'nourut',
        'kode_barang',
        'nama_barang',
        'jumlah',
        'kode_gudang'
    ];
}
