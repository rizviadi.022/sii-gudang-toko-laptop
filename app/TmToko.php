<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TmToko extends Model
{
    protected $table='tm_toko';
    protected $fillable=[
        'kode_toko',
        'nama_toko',
        'alamat',
        'keterangan',
        'aktif'
    ];
}
