<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TempTransIn extends Model
{
    protected $table='temp_transin';
    protected $fillable=[
      'nourut',
      'kode_barang',
      'nama_barang',
      'jumlah',
      'kode_gudang'
    ];
}
