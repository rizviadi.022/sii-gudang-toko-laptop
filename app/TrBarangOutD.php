<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrBarangOutD extends Model
{
    protected $table='tr_barang_out_d';
    protected $fillable=[
        'notrans_barang_out',
        'kode_barang',
        'nama_barang',
        'jumlah',
        'kode_gudang'
    ];
}
