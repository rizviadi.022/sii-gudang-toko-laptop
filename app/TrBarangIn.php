<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrBarangIn extends Model
{
    protected $table='tr_barang_in';
    protected $fillable=[
      'notrans_barang_in',
      'tanggal',
      'no_nota',
      'kode_supplier'
    ];
}
