<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TmGudang extends Model
{
    protected $table='tm_gudang';
    protected $fillable=[
      'kode_gudang',
      'nama_gudang',
      'keterangan',
      'aktif'
    ];
}
