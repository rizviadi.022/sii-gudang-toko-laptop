<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TmSupplier extends Model
{
    protected $table='tm_supplier';
    protected $fillable=[
      'kode_supplier',
      'nama_supplier',
      'alamat',
      'keterangan',
      'aktif'
    ];
}
