<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTmGudangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tm_gudang', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode_gudang',10)->unique();
            $table->string('nama_gudang',25)->nullable();
            $table->string('keterangan',100)->nullable();
            $table->string('aktif',1)->default('Y');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tm_gudang');
    }
}
