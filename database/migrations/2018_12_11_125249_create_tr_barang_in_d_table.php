<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrBarangInDTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tr_barang_in_d', function (Blueprint $table) {
            $table->string('notrans_barang_in',20);
            $table->string('kode_barang',20)->nullable();
            $table->string('nama_barang',25)->nullable();
            $table->decimal('jumlah',3)->nullable();
            $table->string('kode_gudang',10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tr_barang_in_d');
    }
}
