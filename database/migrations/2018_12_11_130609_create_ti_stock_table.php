<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTiStockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ti_stock', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode_barang',20)->unique();
            $table->string('nama_barang',25);
            $table->decimal('stock_akhir',3);
            $table->string('kode_gudang',10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ti_stock');
    }
}
