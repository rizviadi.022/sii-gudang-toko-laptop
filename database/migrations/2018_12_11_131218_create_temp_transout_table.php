<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempTransoutTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_transout', function (Blueprint $table) {
            $table->string('nourut',2);
            $table->string('kode_barang',20);
            $table->string('nama_barang',25);
            $table->decimal('jumlah',3);
            $table->string('kode_gudang',10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_transout');
    }
}
