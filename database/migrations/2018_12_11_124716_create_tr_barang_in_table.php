<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrBarangInTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tr_barang_in', function (Blueprint $table) {
            $table->increments('id');
            $table->string('notrans_barang_in',20)->unique();
            $table->date('tanggal');
            $table->string('no_nota',25)->nullable();
            $table->string('kode_supplier',25)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tr_barang_in');
    }
}
