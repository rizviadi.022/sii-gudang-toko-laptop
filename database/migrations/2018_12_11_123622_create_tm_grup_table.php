<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTmGrupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tm_grup', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode_grup',10)->unique();
            $table->string('nama_grup',25)->nullable();
            $table->string('keterangan',100)->nullable();
            $table->string('aktif',1)->default('Y');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tm_grup');
    }
}
