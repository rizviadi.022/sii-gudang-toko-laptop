<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTmBarangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tm_barang', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode_barang',20)->unique();
            $table->string('nama_barang',25)->nullable();
            $table->string('kode_grup',10);
            $table->string('keterangan',100)->nullable();
            $table->string('default_gd',10);
            $table->string('aktif',1)->default('Y');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tm_barang');
    }
}
